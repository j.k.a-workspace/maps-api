const Stack = createNativeStackNavigator();
import { StyleSheet, SafeAreaView } from "react-native";
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { useFonts } from "expo-font";
import InicioDeSesion from "./screens/InicioDeSesion";
import RutaCentro from "./screens/RutaCentro";
import RutaVillaFontana from "./screens/RutaVillaFontana";
import RutaNatura from "./screens/RutaNatura";

import { createNativeStackNavigator } from "@react-navigation/native-stack";

const App = () => {
  const [hideSplashScreen, setHideSplashScreen] = React.useState(true);

  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        {hideSplashScreen ? (
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen
              name="InicioDeSesion"
              component={InicioDeSesion}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaCentro"
              component={RutaCentro}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaVillaFontana"
              component={RutaVillaFontana}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaNatura"
              component={RutaNatura}
              options={{ headerShown: false }}
            />

          </Stack.Navigator>
        ) : null}
      </NavigationContainer>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 24,
  },
});
export default App;

